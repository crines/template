/*import something here 
run npm i @websanova/vue-auth
*/
import auth from './auth'
import VueAuth from '@websanova/vue-auth'

// "async" is optional
export default ({ app, Vue, router}) => {
  Vue.router = router
  Vue.use(VueAuth,auth)
}
