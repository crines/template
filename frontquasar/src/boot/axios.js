import Vue from 'vue'
import axios from 'axios'

const axiosInstance = axios.create({
    // NOTE discomment this in production mode
    // baseURL += `api/v1`
    //NOTE discomment this in develop mode 
    baseURL: `${process.env.API_URL}api/v1`
  })

Vue.$http = axiosInstance
Vue.prototype.$axios = axiosInstance
//console.log('url',APP_URL)
axios.defaults.baseURL = '/api/v1'
Vue.axios=axiosInstance

