import bearer from '@websanova/vue-auth/drivers/auth/bearer'
import axios from '@websanova/vue-auth/drivers/http/axios.1.x'
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x'

const config = {
  auth: bearer,
  http: axios,
  router: router,
  tokenDefaultName: 'token',
  tokenStore: ['localStorage'],
  rolesVar: 'id_roles',
  authRedirect:{path:'/login'},
  // NOTE User model field which contains the user role details.

  // API endpoints used in Vue Auth.
  registerData: {
    url: 'auth/register', 
    method: 'POST', 
    redirect: '/login'
  },

  loginData: {
    url: 'auth/login',
    method: 'POST',
    redirect: '', 
    fetchUser: false
  },

  logoutData: {
    url: 'auth/logout',
    method: 'POST',
    redirect: '/login',
    makeRequest: false
  },
  
  fetchData: {
    url: 'auth/user',
    method: 'GET',
    enabled: false
  },

  refreshData: {
    url: 'auth/refresh',
    method: 'GET',
    enabled: true,
    interval: 30
  }
}
export default config
