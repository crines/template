
const routes = [
  {
    path: '/',
    component: () => import('layouts/loginLayout.vue'),
    children: [{ 
      path: '', component: () => 
      import('pages/index'),
      meta: {title: "home"}
     }],
    meta:{auth: true}
  },
 
  {
    path: '/login',
    component: () => import('layouts/loginLayout.vue'),
    children: [{ 
      path: '', component: () => 
      import('pages/login'),
    }],
    meta:{auth: false} 
  }


  // Always leave this as last one,
  // but you can also remove it
 /* {
    path: '*',
    component: () => import('pages/Error404.vue')
  }*/
]

// Always leave this as last one

if (process.env.MODE !== 'ssr') {
  routes.push({
      path: '*',
      component: () =>
          import ('pages/system/Error404.vue')
  })
}


export default routes
