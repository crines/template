<?php
//NOTE run php artisan jwt:secret 
  // to generate a private key to sign geenrated tokens

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::prefix('v1')->group(function(){
    Route::prefix('auth')->group(function () {
          //SECTION Routes Outside APP
          Route::post('register', 'UserController@register');
          //Route::post('login', 'UserController@authenticate');
/*  Route::post('register', 'AuthController@register');
    // Login User
    Route::post('login', 'AuthController@login');
    // Refresh the JWT Token
    Route::get('refresh', 'AuthController@refresh');
    //SECTION restore password
    
    // Enviar correo de restablecimiento de contraseña
    Route::post('reset-password', 'AuthController@sendPasswordResetLink');

    // manejar el proceso de restablecimiento de contraseña
    Route::post('reset/password', 'AuthController@callResetPassword');

    Route::post('setlog', 'UserController@setLog');

    Route::middleware('auth:api')->group(function () {
      //SECTION This routes need token to
      Route::get('user', 'AuthController@user');
      // Logout user from application
      Route::post('logout', 'AuthController@logout');
 */
    });    
});



Route::middleware('auth:api')->group(function () {
    //SECTION general endpoints using for all roles
    //Route::resource('user', 'UserController')->only(['index','show']);

/*
    Route::prefix('production')->group(function(){
        //SECTION orders_reception
        Route::get('orders_reception', 'Order_ReceptionController@index');
    });
    
    Route::prefix('sellers')->group(function(){
        Route::get('storeSeller',  'SellerQueryBuilder@showSellerStore');
    });
*/
});
